class ChangeModelsNames < ActiveRecord::Migration[6.0]
  def change
	  rename_table :categories, :projects
	  rename_table :tasks, :todos
  end
end
