class ChangeTodoTableRowNameCategoryToProject < ActiveRecord::Migration[6.0]
  def change
	  rename_column :todos, :category_id, :project_id
  end
end
